package Unit5;

import java.util.Scanner;

public class Board {

	public static final int SHIP_SYMBOL = '1';
	public static final int WATER_SYMBOL = 'O';
	public static final int SUNK_SHIP_SYMBOL = 'X';
	public static final int EMPTY_SYMBOL = '·';
	private int numShips;
	public int MAX_SHOOTS;
	public static int dimension;
	private char[][] matrix;
	private int remainingShoots;
	private int sunkShipCounter;

	Scanner input = new Scanner(System.in);
	private int x;
	private int cont;
	private static int lastrow = 0;
	private static char c = 'A';

	public Board(int size, int numShips) {
		dimension = size;
		this.numShips = numShips;
		matrix = new char[dimension][dimension];
		MAX_SHOOTS= numShips*3;
		remainingShoots = MAX_SHOOTS;
		sunkShipCounter = 0;
		x = (dimension * 2) + 1;
		iniMatrix();
		addShips();
	}

	private void iniMatrix() {
		for (int row = 0; row < matrix.length; row++) {
			for (int col = 0; col < matrix[0].length; col++) {
				matrix[row][col] = EMPTY_SYMBOL;
			}

		}
	}

	private void addShips() {
		cont = 0;
		while (cont <= numShips) {
			int x = (int) (Math.random() * (dimension - 1) + 1);
			int y = (int) (Math.random() * (dimension - 1) + 1);
			if (matrix[y][x] == EMPTY_SYMBOL) {
				matrix[x][y] = SHIP_SYMBOL;
				cont++;
			}
		}
	}

	public void shoots() {
		char letter;
		int num;
		int row;
		int col;
		while (remainingShoots >= 1 && sunkShipCounter < numShips) {
			System.out.print("Enter the row(letter): ");
			letter = input.next().charAt(0);
			row = (letter + 1) - 'A';
			System.out.print("Enter the col(number): ");
			num = input.nextInt();
			col = num;
			System.out.print("\n");
			remainingShoots--;
			if (matrix[row][col] == SHIP_SYMBOL) {
				matrix[row][col] = SUNK_SHIP_SYMBOL;
				sunkShipCounter++;
			} else {
				matrix[row][col] = WATER_SYMBOL;
			}
			lastrow = 0;
			printMatrix();
		}
	}

	public void printMatrix() {
		for (int i = 1; i <= x; i++) {
			if (i == 1) {
				printCabecera();
			} else {
				if (i == x) {
					printPie();
				} else {
					if (i % 2 == 0) {
						printArray();
					} else {
						printSeparacion();
					}
				}
			}

		}
	}

	private void printCabecera() {
		cont = 1;
		while (cont <= x) {
			if (cont == 1) {
				System.out.print("\u2554");// Esquina superior izquierda
				cont++;
			} else {
				if (cont == x) {
					System.out.println("\u2557");// Esquina superior derecha
					cont++;
				} else {
					if (cont % 2 == 0) {
						System.out.print("\u2550" + "\u2550" + "\u2550");// Horintal basica
						cont++;
					} else {
						System.out.print("\u2566");// Horizontal hacia abajo
						cont++;
					}
				}
			}

		}
	}

	private void printPie() {
		cont = 1;
		while (cont <= x) {
			if (cont == 1) {
				System.out.print("\u255A");// Esquina superior izquierda
				cont++;
			} else {
				if (cont == x) {
					System.out.println("\u255D");// Esquina superior derecha
					cont++;
				} else {
					if (cont % 2 == 0) {
						System.out.print("\u2550" + "\u2550" + "\u2550");// Horintal basica
						cont++;
					} else {
						System.out.print("\u2569");// Horizontal hacia abajo
						cont++;
					}
				}
			}

		}
	}

	private void printArray() {
		for (int row = lastrow; row < matrix.length;) {
			if (row == 0) {
				System.out.print("\u2551" + "   ");
			} else {
				System.out.print("\u2551 " + c + " ");
				c++;
			}
			for (int col = 1; col < matrix[0].length; col++) {
				if (row == 0) {
					System.out.print("\u2551 " + col + " ");
				} else {
					if (matrix[row][col] == '1') {
						System.out.print("\u2551" + " · ");
					} else {
						System.out.print("\u2551 " + matrix[row][col] + " ");
					}
				}
			}
			System.out.println("\u2551");
			if (row == (matrix.length - 1)) {
				c = 'A';
			}
			lastrow++;
			break;
		}
	}

	private void printSeparacion() {
		cont = 1;
		while (cont <= x) {
			if (cont == 1) {
				System.out.print("\u2560");// Vertical derecha
				cont++;
			} else {
				if (cont == x) {
					System.out.println("\u2563");// Vertical izquierda
					cont++;
				} else {
					if (cont % 2 == 0) {
						System.out.print("\u2550" + "\u2550" + "\u2550");// Horintal basica
						cont++;
					} else {
						System.out.print("\u256C");// Cruz
						cont++;
					}
				}
			}

		}
	}
}
